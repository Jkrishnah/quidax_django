import os
import django
os.environ['DJANGO_SETTINGS_MODULE'] = 'quidaxapp.settings'
django.setup()
from walletmonitor.models import *
import json
from datetime import datetime,timedelta
import time
import requests
runtime = datetime.utcnow()+timedelta(hours=1)
LastRunTime.objects.last().time = runtime
host = "https://www.quidax.com"
version = "v1"
key = Key.objects.first().apiKey
headers= {"Authorization":f'Token {key}'}
payload = {}
def check_market(market):
    orderbook_url = f"{host}/api/{version}/markets/tickers/{market.market}"
    response = requests.request("GET", orderbook_url, data = payload)
    response_json = json.loads(response.text.encode('utf8'))
    market_balance,ngn_balance=check_balance(market)
    data = response_json["data"]
    buyprice = float(data["ticker"]["buy"])
    sellprice = float(data["ticker"]["sell"])
    if (market.minbalance < market_balance) and ((market.minbalance * sellprice) < ngn_balance) :
        if response_json["status"] == "success":
            buydistance = market.buydistance
            selldistance = market.selldistance
            volume = market.volume
            if ((sellprice - selldistance) - (buyprice + buydistance)) > (0.0065*(sellprice - selldistance)):
                create_buy_order_url = f"{host}/api/{version}/users/me/orders?market={market.market}&side=buy&ord_type=limit&price={buyprice+buydistance}&volume={volume}"
                buy_response = requests.request("POST", create_buy_order_url, headers=headers, data = payload)
                buy_response_id = json.loads(buy_response.text)['data']['id']
                buy_response_status =  json.loads(buy_response.text)['data']['status']
                create_sell_order_url = f"{host}/api/{version}/users/me/orders?market={market.market}&side=sell&ord_type=limit&price={sellprice-selldistance}&volume={volume}"
                sell_response = requests.request("POST", create_sell_order_url, headers=headers, data = payload)
                sell_response_id = json.loads(sell_response.text)['data']['id']
                sell_response_status =  json.loads(sell_response.text)['data']['status']
                time.sleep(market.wait*60)
                buy_order_status = orders(buy_response_id)
                sell_order_status = orders(sell_response_id)
                successEvent.objects.create(buyprice=buyprice,sellprice=sellprice,buy_order_price=buyprice+buydistance,sell_order_price=sellprice-selldistance,volume=volume,buy_order_status=buy_order_status,sell_order_status=sell_order_status,market=market,time=runtime)
                Log.objects.create(status=True,buyprice=buyprice,sellprice=sellprice,buy_order_price=buyprice+buydistance,sell_order_price=sellprice-selldistance,volume=volume,buy_order_status=buy_order_status,sell_order_status=sell_order_status,market=market,time=runtime)
            else:
                Log.objects.create(buyprice=buyprice,sellprice=sellprice,market=market,time=runtime,failure_cause="Condition failed",status=False)
    else:
        Log.objects.create(buyprice=buyprice,sellprice=sellprice,market=market,time=runtime,failure_cause="Insufficient Balance",status=False)
def orders(orderid):
    get_orders_url = f'{host}/api/{version}/users/me/orders/{orderid}'
    orders_response = requests.request("GET", get_orders_url,headers=headers)
    orderstatus = json.loads(orders_response.text)["data"]["status"]
    return orderstatus
def check_balance(market):
    market_name = market.market.replace('ngn','')
    market_balance_check_url = f'{host}/api/{version}/users/me/wallets/{market_name}'
    market_balance_response = requests.request("GET",market_balance_check_url,headers=headers)
    market_balance = json.loads(market_balance_response.text)["data"]["balance"]
    ngn_balance_check_url = f'{host}/api/{version}/users/me/wallets/ngn'
    ngn_balance_response = requests.request("GET",ngn_balance_check_url,headers=headers)
    ngn_balance = json.loads(ngn_balance_response.text)["data"]["balance"]
    return float(market_balance),float(ngn_balance)
for market in Config.objects.all():
    if market.active:
        check_market(market)
    