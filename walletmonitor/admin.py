from django.contrib import admin
from .models import *
from django.contrib.auth.models import Group,User
# from .views import ExportCsvMixin

admin.site.unregister(Group)
admin.site.unregister(User)
# admin.site.register(Log)

@admin.register(Config)
class ConfigAdmin(admin.ModelAdmin):
     readonly_fields = ["market"]
     list_display = ["market","minbalance","buydistance","selldistance","volume","wait","active"]
     fields = ["market","minbalance","buydistance","selldistance","volume","wait","active"]
     def has_add_permission(self, request):
        return False
     def has_delete_permission(self, request, obj=None):
        return False

@admin.register(Log)
class LogAdmin(admin.ModelAdmin):
     list_display = ['time','market','status','failure_cause','buy_order_status','sell_order_status','buyprice','sellprice','buy_order_price','sell_order_price','volume']
     list_filter = ['status','time','market']
     ordering = ['-time']
     def has_add_permission(self, request):
        return False
   #   def has_delete_permission(self, request, obj=None):
   #      return False
@admin.register(successEvent)
class successeventAdmin(admin.ModelAdmin):
     list_display = ['time','market','buy_order_status','sell_order_status','buyprice','sellprice','buy_order_price','sell_order_price','volume']
     list_filter = ['time','market']
     ordering = ['-time']
     def has_add_permission(self, request):
        return False
   #   def has_delete_permission(self, request, obj=None):
   #      return False
@admin.register(LastRunTime)
class lastruntimeAdmin(admin.ModelAdmin):
     def has_add_permission(self, request):
        return False
     def has_delete_permission(self, request, obj=None):
        return False
@admin.register(Key)
class keyAdmin(admin.ModelAdmin):
     def has_add_permission(self, request):
        return False
     def has_delete_permission(self, request, obj=None):
        return False
@admin.register(Interval)
class IntervalAdmin(admin.ModelAdmin):
     def has_add_permission(self, request):
        return False
     def has_delete_permission(self, request, obj=None):
        return False
