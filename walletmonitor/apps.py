from django.apps import AppConfig


class WalletmonitorConfig(AppConfig):
    name = 'walletmonitor'
