# Generated by Django 3.0.7 on 2020-06-19 14:40

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('walletmonitor', '0001_initial'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Templates',
            new_name='Config',
        ),
    ]
