# Generated by Django 3.0.7 on 2020-07-09 11:30

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('walletmonitor', '0024_config_wait'),
    ]

    operations = [
        migrations.AlterField(
            model_name='config',
            name='wait',
            field=models.FloatField(),
        ),
    ]
