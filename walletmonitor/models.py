from django.db import models
from django.db.models.signals import post_save
from django.db.models.signals import pre_save
from .utils import cron_handler
class Config(models.Model):
    market = models.CharField(max_length=3000)
    buydistance = models.FloatField()
    selldistance = models.FloatField()
    minbalance=models.FloatField(default=None)
    volume = models.FloatField(default=None)
    active = models.BooleanField(default=True)
    wait = models.FloatField()
    
    def __str__(self):
        return f'{self.market}'
class Price(models.Model):
    market = models.ForeignKey(Config,on_delete=models.CASCADE)
    price = models.FloatField()
    time = models.DateTimeField()
    def __str__(self):
        return f'{self.market}'
class Key(models.Model):
    apiKey = models.CharField(max_length=600)
    def __str__(self):
        return f'API Key'
class successEvent(models.Model):
    market = models.ForeignKey(Config,on_delete=models.CASCADE)
    buy_order_status = models.CharField(max_length=100)
    sell_order_status = models.CharField(max_length=100)
    buyprice = models.FloatField()
    sellprice = models.FloatField()
    buy_order_price = models.FloatField()
    sell_order_price = models.FloatField()
    volume = models.FloatField()
    time = models.DateTimeField()
class Log(models.Model):
    market = models.ForeignKey(Config,on_delete=models.SET_NULL,null=True)
    status =models.BooleanField()
    buy_order_status = models.CharField(max_length=100,null=True)
    sell_order_status = models.CharField(max_length=100,null=True)
    buyprice = models.FloatField(null=True)
    sellprice = models.FloatField(null=True)
    buy_order_price = models.FloatField(null=True)
    sell_order_price = models.FloatField(null=True)
    volume = models.FloatField(null=True)
    time = models.DateTimeField()
    failure_cause = models.CharField(max_length=1000,null=True)
    def __str__(self):
        return f'{self.market}'
class LastRunTime(models.Model):
    time = models.DateTimeField()
    def __str__(self):
        return f'{self.time}'

class Interval(models.Model):
    interval = models.IntegerField()
    run = models.BooleanField(default=True)
    def __str__(self):
        return f'{self.interval} Minutes'
#triggers
post_save.connect(cron_handler, sender=Interval)