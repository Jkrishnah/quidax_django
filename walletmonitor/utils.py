from crontab import CronTab
import datetime
import croniter
from django.db.models.signals import post_save
from django.dispatch import receiver
from .models import *
def cron_handler(sender, instance, *args, **kwargs):
    cron = CronTab(user='root')
    found=0
    for job in cron:
        if job.comment == 'quidax_cron':
            if instance.run:
                job.minute.every(instance.interval)
                cron.write()
                found = 1
            else:
                found = 1
                cron.remove(job)
                cron.write()
    if found:
        pass
    else:
        if instance.run:
            job = cron.new(command='/root/jai/quidax-app/env/bin/python /root/jai/quidax-app/quidaxapp/schedule.py', comment='quidax_cron')
            job.minute.every(instance.interval)
            cron.write()
        else:
            pass
    # /root/jai/quidax-app/env/bin/python
    # /root/jai/quidax-app/quidaxapp/walletmonitor/schedule.py